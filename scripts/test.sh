#!/usr/bin/env bash
#
# Run unit and functional tests
#

set -eu -o pipefail

for package in $(find . -name '*.go' | egrep -o '.*/' | sort | uniq); do
    go test -v ${package}

    if [[ $? != 0 ]]; then
         exit 2
    fi
done