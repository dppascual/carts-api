#!/usr/bin/env bash
#
# Build a static binary for the host OS/ARCH
#

set -eu -o pipefail

source ./scripts/.variables

TARGET="build/${2}-${VERSION}-${GOOS}-${GOARCH}"

echo "Building statically linked $TARGET"
export CGO_ENABLED=0
go build -o "${TARGET}" --ldflags "${LDFLAGS}" $1

ln -sf "$(basename "${TARGET}")" "build/${2}"