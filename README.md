# Carts API

- [Introduction](#introduction)
  - [Configurations](#configurations)
- [Getting started](#getting-started)
  - [How to build a binary](#how-to-build-a-binary)
  - [How to start up a development environment](#how-to-start-up-a-development-environment)
  - [How to test the API server](#how-to-test-the-api-server)
  
## Introduction

The `Carts API` exposes a simple checkout service that allows you to easily manage carts and cart items.

The API specification can be found in the link below.

> URL: [Carts API specification](https://app.swaggerhub.com/apis-docs/dppascual/carts-api/1.0.0)

The repository also includes a simple client to be able to interact with the API server. More details can be found in the 
following sections.

#### Configurations

You can pass the following environment variables to the `API server`:

* `LANA_DAEMON_PORT`: Variable that tells the server to accept incoming requests only on the specified port
                      (defaults to `8080`).

* `LANA_READ_TIMEOUT`: Maximum duration for reading the entire request, including the body (defaults to `5s`).

* `LANA_WRITE_TIMEOUT`: Maximum duration before timing out writes of the response (defaults to `10s`).

* `LANA_SHUTDOWN_TIMEOUT`: Maximum duration for shutting down gracefully the server once an exit signal or error is 
                           received. When the timeout is exceeded, a signal `SIGKILL` is sent to the server
                           (defaults to `5s`).
            
Environment variables that can be passed to the `client`:

* `LANA_DAEMON_HOST`: The default hostname used by the command-line client (defaults to `localhost`).

* `LANA_DAEMON_PORT`: The default port used by the command-line client (defaults to `8080`).

## Getting started

Before building the `server`, several requirements must be met:

1.- Docker is used during the building process. It can be installed by following the steps at the link below.

> INFO: [How to install a Docker Community Edition](https://docs.docker.com/install/)

2.- Docker Compose is used when a testing or development environment is launched. It can be installed by following the 
steps at the link below.

> INFO: [How to install Docker Compose](https://docs.docker.com/compose/install/)

3.- In order to simplify the process, all commands are saved and executed under the `make` command, and hence, it 
must be installed as well.

How to install `make` on several OS platforms:

> LINUX (UBUNTU)

```bash
$ sudo apt update
$ sudo apt install build-essential
```

> LINUX (RED HAT/CENTOS)

```bash
$ sudo yum -y update
$ sudo yum -y install make
```

> MAC

```bash
xcode-select --install
```

### Quickstart

#### How to build a binary

Once those two tools are installed, a set of Dockerfiles placed in the repository are used to create a Docker image 
with all required tools for creating a Linux binary.

```bash
$ git clone git@github.com:dppascual/carts-api.git
$ cd carts-api && make build
```

After you execute the above command, a Linux binary is saved on the cloned repository folder `build`. From now, you’re 
ready to start up the `server`.

```bash
$ ./build/server
```

#### How to start up a development environment

The `make` command comes with a set of options that can be used for development or testing purposes. Among others, the 
option `shell` or `dev` starts an entire development environment with all services required to simulate a production 
environment along with an interactive session where the client can be tested.

```bash
bash-5.0# lanactl help
Description:
  Command line interface for Lana API Server

  All of API operations can be driven through the various commands below.
  For help with any of those, simply call them with --help.

Usage:
  lanactl [command]

Available Commands:
  carts       Display details of a set of carts or a single one
  help        Help about any command

Flags:
      --all              Show all commands
  -h, --help             help for lanactl
  -v, --verbose string   Log level (debug, info, warn, error, fatal, panic (default "info")

Use "lanactl [command] --help" for more information about a command.
```

List of all available targets:

```bash
help                 print this help
build-server         build the api server
build-client         build the client
test                 run unit and functional tests
dev                  start a development environment in interactive mode
dev-stop             stop a development environment
dev-status           display the status of container associated to a development environment
shell                alias for dev
shell-stop           alias for dev-stop
shell-status         alias for dev-status
authors              generate AUTHORS file from git history
```

### How to test the API server

Testing software in all forms is extremely important, whether it be unit, integration, system, or acceptance testing. 
However, depending on the project, one form of testing can lead to better understanding about the health and integrity 
of the software than the other forms.

When developing a tool which depends on third party services, a strong set of integration tests can provide a better 
understanding of the service than other types of tests. Integration tests are a form of software testing that tests 
the interaction of your code against the dependencies your application is leveraging. Without integration tests, 
it’s difficult to trust the end-to-end operation of a web service. Individual units of code rarely provide the same 
level of insight.

Having said it, a complete set of unit and functional tests has been implemented and can be executed from the command line 
interface or, automatically, when an event `synchronize` or `push` triggers a CI workflow on Github.

```bash
$ make test
```

