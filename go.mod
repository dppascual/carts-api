module github.com/dppascual/carts-api

go 1.13

require (
	github.com/google/go-cmp v0.4.0
	github.com/gorilla/mux v1.7.3
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/konsorten/go-windows-terminal-sequences v1.0.2 // indirect
	github.com/sirupsen/logrus v1.4.2
	github.com/spf13/cobra v0.0.5
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/stretchr/testify v1.4.0
	golang.org/x/sys v0.0.0-20200117145432-59e60aa80a0c // indirect
	gopkg.in/yaml.v2 v2.2.7 // indirect
)
