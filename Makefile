#
# https://github.com/dppascual/lana-carts
#
# Makefile for developing using Docker
#

DOCKER_LANA_MOUNTS ?= -v "$(shell pwd)":/project
DOCKER_LANA_CONTAINER_NAME ?=

BINARY_DOCKER_IMAGE_NAME = lana/server

.PHONY: help
help: ## print this help
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z0-9_-]+:.*?## / {gsub("\\\\n",sprintf("\n%22c",""), $$2);printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.PHONY: build_binary_image
build_binary_image:
# build dockerfile from file so that we have to send the build-context.
	@docker build -q -t $(BINARY_DOCKER_IMAGE_NAME) -f dockerfiles/Dockerfile.binary .

DOCKER_RUN_NAME_OPTION := $(if $(DOCKER_LANA_CONTAINER_NAME),--name $(DOCKER_LANA_CONTAINER_NAME) --hostname $(DOCKER_LANA_CONTAINER_NAME),)
DOCKER_RUN := docker run --rm $(ENVVARS) $(DOCKER_LANA_MOUNTS) $(DOCKER_RUN_NAME_OPTION)

.PHONY: build-server
build-server: build_binary_image ## build the api server
	@$(DOCKER_RUN) $(BINARY_DOCKER_IMAGE_NAME) cmd/server/main.go server

.PHONY: build-client
build-client: build_binary_image ## build the client
	@$(DOCKER_RUN) $(BINARY_DOCKER_IMAGE_NAME) cmd/client/main.go lanactl

.PHONY: test
test: build_binary_image ## run unit and functional tests
	@$(DOCKER_RUN) --entrypoint ./scripts/test.sh $(BINARY_DOCKER_IMAGE_NAME)

.PHONY: dev
dev: ## start a development environment in interactive mode
	@docker-compose -f dockerfiles/docker-compose-dev.yml up -d --build && sleep 5 && docker exec -it command-line /bin/bash

.PHONY: dev-stop
dev-stop: ## stop a development environment
	@docker-compose -f dockerfiles/docker-compose-dev.yml down --volumes

.PHONY: dev-status
dev-status: ## display the status of container associated to a development environment
	@docker-compose -f dockerfiles/docker-compose-dev.yml ps

.PHONY: shell shell-stop shell-status
shell: dev ## alias for dev
shell-stop: dev-stop ## alias for dev-stop
shell-status: dev-status ## alias for dev-status

.PHONY: authors
authors: build_binary_image ## generate AUTHORS file from git history
	@$(DOCKER_RUN) --entrypoint ./scripts/authors.sh $(BINARY_DOCKER_IMAGE_NAME)

.PHONY: clean ## clean up the folder build
clean:
	@echo "Cleaning up the folder build..."
	@rm -rf build/*