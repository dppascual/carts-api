package tests

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/dppascual/carts-api/handlers"
	"github.com/google/go-cmp/cmp"
	"github.com/stretchr/testify/assert"
)

func Test_Carts(t *testing.T) {

	// No content (no seed data)
	{
		req, err := http.NewRequest(http.MethodGet, "/carts", nil)
		assert.Nil(t, err)

		w := httptest.NewRecorder()
		app.ServeHTTP(w, req)
		assert.Equal(t, http.StatusOK, w.Code)

		if len(w.Body.Bytes()) > 1 {
			t.Errorf("expected no carts to be returned")
		}
	}

	// Seeding data
	{
		expectedCart := app.SetStore(storeData)
		defer app.Truncate()

		req, err := http.NewRequest(http.MethodGet, "/carts", nil)
		assert.Nil(t, err)

		w := httptest.NewRecorder()
		app.ServeHTTP(w, req)
		assert.Equal(t, http.StatusOK, w.Code)

		var carts []handlers.CartResource

		err = json.NewDecoder(w.Body).Decode(&carts)
		assert.Nil(t, err)

		for _, v := range carts {
			for _, vExpected := range expectedCart {
				if v.ID == vExpected.ID {
					if d := cmp.Diff(vExpected, v); d != "" {
						t.Errorf("unexpected difference in response body:\n%v", d)
					}
				}
			}
		}
	}
}

func Test_CreateCart(t *testing.T) {

	defer app.Truncate()

	testTable := []struct {
		Name         string
		Path         string
		ExpectedCode int
	}{
		{
			Name:         "OK",
			Path:         "/carts",
			ExpectedCode: http.StatusCreated,
		},
	}

	for _, tt := range testTable {
		t.Run(tt.Name, func(t *testing.T) {

			req, err := http.NewRequest(http.MethodPost, tt.Path, nil)
			assert.Nil(t, err)

			w := httptest.NewRecorder()
			app.ServeHTTP(w, req)

			assert.Equal(t, tt.ExpectedCode, w.Code)
		})
	}
}

func Test_CartByID(t *testing.T) {

	defer app.Truncate()

	app.SetStore(storeData)

	testTable := []struct {
		Name         string
		CartID       string
		ExpectedBody handlers.CartResource
		ExpectedCode int
	}{
		{
			Name:         "OK",
			CartID:       storeData["1"].ID,
			ExpectedBody: storeData["1"],
			ExpectedCode: http.StatusOK,
		},
		{
			Name: "NotFound",
			// Using 0 for ID because serial type starts at 1 so 0 will never exist.
			CartID:       "0",
			ExpectedBody: handlers.CartResource{},
			ExpectedCode: http.StatusNotFound,
		},
	}

	for _, tt := range testTable {
		t.Run(tt.Name, func(t *testing.T) {
			req, err := http.NewRequest(http.MethodGet, fmt.Sprintf("/carts/%s", tt.CartID), nil)
			assert.Nil(t, err)

			w := httptest.NewRecorder()
			app.ServeHTTP(w, req)
			assert.Equal(t, tt.ExpectedCode, w.Code)

			if tt.ExpectedCode != http.StatusNotFound {
				var cart []handlers.CartResource

				err := json.NewDecoder(w.Body).Decode(&cart)
				assert.Nil(t, err)

				if d := cmp.Diff(tt.ExpectedBody, cart[0]); d != "" {
					t.Errorf("unexpected difference in response body:\n%v", d)
				}
			}
		})
	}
}

func Test_deleteCart(t *testing.T) {
	defer app.Truncate()

	app.SetStore(storeData)

	testTable := []struct {
		Name         string
		CartID       string
		ExpectedCode int
	}{
		{
			Name:         "OK",
			CartID:       storeData["1"].ID,
			ExpectedCode: http.StatusNoContent,
		},
		{
			Name: "NotFound",
			// Using 0 for ID because serial type starts at 1 so 0 will never exist.
			CartID:       "0",
			ExpectedCode: http.StatusNotFound,
		},
	}

	for _, tt := range testTable {
		t.Run(tt.Name, func(t *testing.T) {
			req, err := http.NewRequest(http.MethodDelete, fmt.Sprintf("/carts/%s", tt.CartID), nil)
			assert.Nil(t, err)

			w := httptest.NewRecorder()
			app.ServeHTTP(w, req)
			assert.Equal(t, tt.ExpectedCode, w.Code)
		})
	}
}
