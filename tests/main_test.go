package tests

import (
	"os"
	"testing"

	"github.com/dppascual/carts-api/handlers"
)

var (
	// app is a reference to the main Application type. This is used for the route
	// definitions that are defined on the embedded handler (Gorilla Mux is used).
	app *handlers.App

	// storeData for seeding data
	storeData = map[string]handlers.CartResource{
		"1": {
			ID: "1",
			Items: []handlers.ItemResource{
				{
					Code:     "PEN",
					Quantity: 1,
				},
				{
					Code:     "TSHIRT",
					Quantity: 1,
				},
				{
					Code:     "MUG",
					Quantity: 1,
				},
			},
			ItemsTotal: 32.5,
			Total:      3,
		},
		"2": {
			ID: "2",
			Items: []handlers.ItemResource{
				{
					Code:     "PEN",
					Quantity: 2,
				},
				{
					Code:     "TSHIRT",
					Quantity: 1,
				},
			},
			ItemsTotal: 25,
			Total:      3,
		},
		"3": {
			ID: "3",
			Items: []handlers.ItemResource{
				{
					Code:     "PEN",
					Quantity: 1,
				},
				{
					Code:     "TSHIRT",
					Quantity: 4,
				},
			},
			ItemsTotal: 65,
			Total:      5,
		},
		"4": {
			ID: "4",
			Items: []handlers.ItemResource{
				{
					Code:     "PEN",
					Quantity: 3,
				},
				{
					Code:     "TSHIRT",
					Quantity: 3,
				},
				{
					Code:     "MUG",
					Quantity: 1,
				},
			},
			ItemsTotal: 62.5,
			Total:      7,
		},
	}
)

func TestMain(m *testing.M) {

	// Declare a new App
	app = handlers.NewApp()

	// Start goroutine responsible for handling interaction with
	// the carts map
	// TODO: Remove when the map stored in memory to be substituted
	//  by a database.
	go app.StartManager()

	os.Exit(m.Run())
}
