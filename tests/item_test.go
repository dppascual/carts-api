package tests

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/dppascual/carts-api/handlers"
	"github.com/stretchr/testify/assert"
)

//// NOTE
// The most relevant HTTP verb associated to an item endpoints is tested.
// The rest ones are very similar to those which are tested over the carts endpoints.

func Test_UpdateItem(t *testing.T) {
	defer app.Truncate()

	expectedCarts := app.SetStore(storeData)

	testTable := []struct {
		Name        string
		CartID      string
		CodeItem    string
		RequestBody struct {
			Quantity int
		}
		ExpectedCode int
	}{
		{
			Name:         "OK",
			CartID:       expectedCarts[0].ID,
			CodeItem:     "PEN",
			RequestBody:  struct{ Quantity int }{Quantity: 3},
			ExpectedCode: http.StatusNoContent,
		},
		{
			Name:         "ErrorCodeItem",
			CartID:       expectedCarts[0].ID,
			CodeItem:     "SHIRT",
			RequestBody:  struct{ Quantity int }{},
			ExpectedCode: http.StatusBadRequest,
		},
		{
			Name: "CartIDNotFound",
			// Using 0 for ID because serial type starts at 1 so 0 will never exist.
			CartID:       "0",
			CodeItem:     "TSHIRT",
			RequestBody:  struct{ Quantity int }{},
			ExpectedCode: http.StatusNotFound,
		},
		{
			Name:         "ItemNotFound",
			CartID:       expectedCarts[1].ID,
			CodeItem:     "MUG",
			RequestBody:  struct{ Quantity int }{Quantity: 1},
			ExpectedCode: http.StatusNotFound,
		},
	}

	for _, tt := range testTable {
		t.Run(tt.Name, func(t *testing.T) {

			var b bytes.Buffer
			err := json.NewEncoder(&b).Encode(tt.RequestBody)
			assert.Nil(t, err)

			req, err := http.NewRequest(http.MethodPatch, fmt.Sprintf("/carts/%s/items/%s", tt.CartID, tt.CodeItem), &b)
			assert.Nil(t, err)

			defer func() {
				if err := req.Body.Close(); err != nil {
					t.Errorf("error encountered closing request body: %v", err)
				}
			}()

			w := httptest.NewRecorder()
			app.ServeHTTP(w, req)
			assert.Equal(t, tt.ExpectedCode, w.Code)

			if tt.ExpectedCode == http.StatusNoContent {
				req, err := http.NewRequest(http.MethodGet, fmt.Sprintf("/carts/%s", tt.CartID), nil)
				assert.Nil(t, err)
				var cart []handlers.CartResource

				w := httptest.NewRecorder()
				app.ServeHTTP(w, req)
				assert.Equal(t, http.StatusOK, w.Code)

				err = json.NewDecoder(w.Body).Decode(&cart)
				assert.Nil(t, err)

				var item handlers.ItemResource
				for _, value := range cart[0].Items {
					if value.Code == tt.CodeItem {
						item = value
					}
				}

				assert.Equal(t, tt.RequestBody.Quantity, item.Quantity)
			}
		})
	}
}
