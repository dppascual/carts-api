package handlers

import (
	"encoding/json"
	"net/http"
	"time"

	log "github.com/sirupsen/logrus"
)

// PRODUCTS
var (
	products = map[string]struct {
		Name  string
		Price float64
	}{
		"PEN": {
			"Lana Pen",
			5.00,
		},
		"TSHIRT": {
			"Lana T-Shirt",
			20.00,
		},
		"MUG": {
			"Lana Coffee Mug",
			7.50,
		},
	}
)

// CartResource is used to hold all data needed to represent a cart.
type CartResource struct {
	ID         string         `json:"id"`
	Items      []ItemResource `json:"items"`
	ItemsTotal float64        `json:"itemsTotal"`
	Total      int            `json:"total"`
}

// ItemResource is used to hold all data needed to represent an item.
type ItemResource struct {
	Code     string `json:"code"`
	Quantity int    `json:"quantity"`
}

// action struct is used to send data to the goroutine managing the state (map) of carts.
type action struct {
	h   http.Handler
	rw  http.ResponseWriter
	req *http.Request
}

// Response struct consists of all the information required to create the correct HTTP Response.
type Response struct {
	StatusCode int
	Carts      []CartResource
}

// ResponseError struct consists of all the information required to create the correct HTTP error
// Response.
type ResponseError struct {
	StatusCode int    `json:"code"`
	Message    string `json:"message"`
}

// loggingResponseWrite is used to capture the status of the request.
// It cannot be fetched through the interface ResponseWrite since such
// value is not exposed.
type loggingResponseWriter struct {
	http.ResponseWriter
	statusCode int
}

func NewLoggingResponseWriter(w http.ResponseWriter) *loggingResponseWriter {
	return &loggingResponseWriter{w, http.StatusOK}
}

func (lrw *loggingResponseWriter) WriteHeader(code int) {
	lrw.statusCode = code
	lrw.ResponseWriter.WriteHeader(code)
}

// RequestMW is a middleware that logs the start and end of each request.
// Also it sends a handler to a sync channel to make sure that just one
// request is managed at a time and no race conditions happen.
//
// All request need to get access a map which is not safe-thread.
// TODO: When a database is used, this channel must be removed
func RequestMW(h http.Handler, ch chan<- action, done <-chan struct{}) http.Handler {

	fn := func(rw http.ResponseWriter, req *http.Request) {
		start := time.Now()
		lrw := NewLoggingResponseWriter(rw)

		// Register the request
		defer func() {
			log.WithFields(log.Fields{
				"method":      req.Method,
				"requestURI":  req.URL.Path,
				"requestTime": time.Since(start),
				"status":      lrw.statusCode,
			}).Info("processed request")
		}()

		ch <- action{h, lrw, req}
		<-done
	}

	return http.HandlerFunc(fn)
}

// writeResponse shows a common pattern used reduce duplicated code.
// It returns a HTTP Response in JSON format.
func writeResponse(w http.ResponseWriter, resp Response) {

	if resp.StatusCode == http.StatusNoContent || resp.Carts == nil {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(resp.StatusCode)
		return
	}

	serializedPayload, err := json.Marshal(resp.Carts)
	if err != nil {
		writeError(w, http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(resp.StatusCode)
	if _, err := w.Write(serializedPayload); err != nil {
		log.WithFields(log.Fields{
			"msgError": err,
		}).Error("Cannot copy the serialized JSON to the Response body")
	}
}

// writeError allows us to return error message in JSON format.
func writeError(w http.ResponseWriter, statusCode int) {
	eResp := ResponseError{
		StatusCode: statusCode,
		Message:    http.StatusText(statusCode),
	}

	serializedPayload, err := json.Marshal(eResp)
	if err != nil {
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statusCode)
	if _, err := w.Write(serializedPayload); err != nil {
		log.WithFields(log.Fields{
			"msgError": err,
		}).Error("Cannot copy the serialized JSON to the Response body")
	}
}
