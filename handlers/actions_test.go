package handlers

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCalcPrice(t *testing.T) {

	testTable := []struct {
		Name               string
		Input              CartResource
		ExpectedTotal      int
		ExpectedItemsTotal float64
	}{
		{
			Name: "Test1",
			Input: CartResource{
				ID: "1",
				Items: []ItemResource{
					{
						Code:     "PEN",
						Quantity: 1,
					},
					{
						Code:     "TSHIRT",
						Quantity: 1,
					},
					{
						Code:     "MUG",
						Quantity: 1,
					},
				},
			},
			ExpectedTotal:      3,
			ExpectedItemsTotal: 32.5,
		},
		{
			Name: "Test2",
			Input: CartResource{
				ID: "2",
				Items: []ItemResource{
					{
						Code:     "PEN",
						Quantity: 2,
					},
					{
						Code:     "TSHIRT",
						Quantity: 1,
					},
				},
				ItemsTotal: 25,
				Total:      3,
			},
			ExpectedTotal:      3,
			ExpectedItemsTotal: 25,
		},
		{
			Name: "Test3",
			Input: CartResource{
				ID: "3",
				Items: []ItemResource{
					{
						Code:     "PEN",
						Quantity: 1,
					},
					{
						Code:     "TSHIRT",
						Quantity: 4,
					},
				},
			},
			ExpectedTotal:      5,
			ExpectedItemsTotal: 65,
		},
		{
			Name: "Test4",
			Input: CartResource{
				ID: "4",
				Items: []ItemResource{
					{
						Code:     "PEN",
						Quantity: 3,
					},
					{
						Code:     "TSHIRT",
						Quantity: 3,
					},
					{
						Code:     "MUG",
						Quantity: 1,
					},
				},
			},
			ExpectedTotal:      7,
			ExpectedItemsTotal: 62.5,
		},
	}

	for _, tt := range testTable {
		t.Run(tt.Name, func(t *testing.T) {

			calcPrice(&tt.Input)
			assert.Equal(t, tt.ExpectedTotal, tt.Input.Total)
			assert.Equal(t, tt.ExpectedItemsTotal, tt.Input.ItemsTotal)
		})
	}
}
