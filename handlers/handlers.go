package handlers

import (
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

// App is the struct that contains the server handler.
type App struct {
	// This channel allows to synchronize access to a map
	// when multiples request are received. A map is not
	// safe-thread. A mutex could be used to perform the
	// very same work.
	syncCh  chan action
	doneCh  chan struct{}
	iStore  map[string]CartResource
	lastID  string
	handler http.Handler
}

// ServeHTTP implements the http.Handler interface for the App type.
func (a *App) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	a.handler.ServeHTTP(w, r)
}

// StartManager starts a goroutine that changes the state of carts (map).
// Primary reason to use a goroutine instead of directly manipulating the carts map is
// to ensure that we do not have multiple requests changing carts' state simultaneously.
func (a *App) StartManager() {
	for c := range a.syncCh {
		c.h.ServeHTTP(c.rw, c.req)
		a.doneCh <- struct{}{}
	}
}

// SetStore allows to set a new store for testing purpose.
func (a *App) SetStore(store map[string]CartResource) []CartResource {

	// Create the target map
	targetMap := make(map[string]CartResource)

	// Copy from the original map to the target map
	for key, value := range store {
		targetMap[key] = value
	}

	a.iStore = targetMap

	var cartResult []CartResource

	for i := 1; i <= len(a.iStore); i++ {
		cartResult = append(cartResult, a.iStore[strconv.Itoa(i)])
	}

	return cartResult
}

// Truncate resets the store and sets it to an empty one. It uses
// for testing purpose.
func (a *App) Truncate() {
	a.iStore = make(map[string]CartResource)
}

// NewApp return a new pointer value to an App type with route
// definitions initiated.
func NewApp() *App {

	app := &App{
		syncCh: make(chan action),
		doneCh: make(chan struct{}),
		iStore: make(map[string]CartResource),
		lastID: "1",
	}

	// Declare a new router
	router := mux.NewRouter()

	// Cart routes
	router.HandleFunc("/carts", app.carts).Methods("GET")
	router.HandleFunc("/carts", app.createCarts).Methods("POST")
	router.HandleFunc("/carts/{id:[0-9]+}", app.cartsById).Methods("GET")
	router.HandleFunc("/carts/{id:[0-9]+}", app.deleteCarts).Methods("DELETE")

	// Item routes
	router.HandleFunc("/carts/{cartId:[0-9]+}/items", app.addItem).Methods("POST")
	router.HandleFunc("/carts/{cartId:[0-9]+}/items/{code:[A-Z]{3,6}}", app.updateItem).Methods("PATCH")
	router.HandleFunc("/carts/{cartId:[0-9]+}/items/{code:[A-Z]{3,6}}", app.deleteItem).Methods("DELETE")

	app.handler = RequestMW(router, app.syncCh, app.doneCh)

	return app
}
