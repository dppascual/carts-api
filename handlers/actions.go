package handlers

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
)

func (a *App) carts(w http.ResponseWriter, r *http.Request) {

	var cartResult []CartResource

	for _, v := range a.iStore {
		cartResult = append(cartResult, v)
	}

	writeResponse(w, Response{
		StatusCode: http.StatusOK,
		Carts:      cartResult,
	})
}

func (a *App) createCarts(w http.ResponseWriter, r *http.Request) {

	// Add a new cart element
	cart := CartResource{
		ID:    a.lastID,
		Items: []ItemResource{},
	}

	a.iStore[a.lastID] = cart

	// Increment the identifier by one
	id, _ := strconv.Atoi(a.lastID)
	id += 1
	a.lastID = strconv.Itoa(id)

	writeResponse(w, Response{
		StatusCode: http.StatusCreated,
		Carts:      []CartResource{cart},
	})
}

func (a *App) cartsById(w http.ResponseWriter, r *http.Request) {

	vPath := mux.Vars(r)

	id, ok := vPath["id"]
	if !ok {
		writeError(w, http.StatusBadRequest)
		return
	}

	cart, ok := a.iStore[id]
	if !ok {
		writeError(w, http.StatusNotFound)
		return
	}

	writeResponse(w, Response{
		StatusCode: http.StatusOK,
		Carts:      []CartResource{cart},
	})
}

func (a *App) deleteCarts(w http.ResponseWriter, r *http.Request) {

	vPath := mux.Vars(r)

	id, ok := vPath["id"]
	if !ok {
		writeError(w, http.StatusBadRequest)
		return
	}

	if _, ok := a.iStore[id]; !ok {
		writeError(w, http.StatusNotFound)
		return
	}

	delete(a.iStore, id)
	writeResponse(w, Response{
		StatusCode: http.StatusNoContent,
		Carts:      nil,
	})
}

func (a *App) addItem(w http.ResponseWriter, r *http.Request) {

	vPath := mux.Vars(r)

	// Check path parameters
	cartId, ok := vPath["cartId"]
	if !ok {
		writeError(w, http.StatusBadRequest)
		return
	}

	// Check if the cart ID exists
	cart, ok := a.iStore[cartId]
	if !ok {
		writeError(w, http.StatusNotFound)
		return
	}

	// Read body
	b, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		writeError(w, http.StatusInternalServerError)
		log.WithFields(log.Fields{
			"msgError": err,
		}).Error("the request body cannot be read")
		return
	}

	// Unmarshal
	msg := struct {
		Code     string `json:"variant"`
		Quantity int    `json:"quantity"`
	}{}
	if err := json.Unmarshal(b, &msg); err != nil {
		writeError(w, http.StatusInternalServerError)
		log.WithFields(log.Fields{
			"msgError": err,
		}).Error("the request body cannot be decoded")
		return
	}

	// Check if the code is valid
	_, ok = products[msg.Code]
	if !ok {
		writeError(w, http.StatusBadRequest)
		log.WithFields(log.Fields{
			"msgError": "the item code is not valid",
		}).Error("error detected by custom checking")
		return
	}

	// Check if the code already exists in the cart
	for _, value := range cart.Items {
		if value.Code == msg.Code {
			writeError(w, http.StatusBadRequest)
			log.WithFields(log.Fields{
				"msgError": "the item to be added already exists in the cart",
			}).Error("error detected by custom checking")
			return
		}
	}

	// Add the new item
	cart.Items = append(cart.Items, ItemResource{
		Code:     msg.Code,
		Quantity: msg.Quantity,
	})

	// Re-calculate price and number of items
	calcPrice(&cart)
	a.iStore[cartId] = cart

	writeResponse(w, Response{
		StatusCode: http.StatusCreated,
		Carts:      []CartResource{cart},
	})
}

func (a *App) updateItem(w http.ResponseWriter, r *http.Request) {

	vPath := mux.Vars(r)

	// Check path parameters
	cartId, ok := vPath["cartId"]
	if !ok {
		writeError(w, http.StatusBadRequest)
		return
	}
	code, ok := vPath["code"]
	if !ok {
		writeError(w, http.StatusBadRequest)
		return
	}

	// Check if the cart ID exists
	cart, ok := a.iStore[cartId]
	if !ok {
		writeError(w, http.StatusNotFound)
		return
	}

	// Check if the code is valid
	_, ok = products[code]
	if !ok {
		writeError(w, http.StatusBadRequest)
		log.WithFields(log.Fields{
			"msgError": "the item code is not valid",
		}).Error("error detected by custom checking")
		return
	}

	var (
		found bool
		idx   int
	)

	for i, value := range cart.Items {
		if value.Code == code {
			found = true
			idx = i
			break
		}
	}

	if found {
		// Read body
		b, err := ioutil.ReadAll(r.Body)
		defer r.Body.Close()
		if err != nil {
			writeError(w, http.StatusInternalServerError)
			log.WithFields(log.Fields{
				"msgError": err,
			}).Error("the request body cannot be read")
			return
		}

		// Unmarshal
		msg := struct {
			Quantity int `json:"quantity"`
		}{}
		if err := json.Unmarshal(b, &msg); err != nil {
			writeError(w, http.StatusInternalServerError)
			log.WithFields(log.Fields{
				"msgError": err,
			}).Error("the request body cannot be decoded")
			return
		}

		cart.Items[idx].Quantity = msg.Quantity
		// Re-calculate price and number of items
		calcPrice(&cart)
		a.iStore[cartId] = cart

		writeResponse(w, Response{
			StatusCode: http.StatusNoContent,
			Carts:      nil,
		})
	} else {
		writeError(w, http.StatusNotFound)
	}
}

func (a *App) deleteItem(w http.ResponseWriter, r *http.Request) {

	vPath := mux.Vars(r)

	// Check path parameters
	cartId, ok := vPath["cartId"]
	if !ok {
		writeError(w, http.StatusBadRequest)
		return
	}
	code, ok := vPath["code"]
	if !ok {
		writeError(w, http.StatusBadRequest)
		return
	}

	// Check if the cart ID exists
	cart, ok := a.iStore[cartId]
	if !ok {
		writeError(w, http.StatusNotFound)
		return
	}

	// Check if the code is valid
	_, ok = products[code]
	if !ok {
		writeError(w, http.StatusBadRequest)
		log.WithFields(log.Fields{
			"msgError": "the item code is not valid",
		}).Error("error detected by custom checking")
		return
	}

	var found bool
	for idx, value := range cart.Items {
		if value.Code == code {
			cart.Items[idx] = cart.Items[len(cart.Items)-1] // Copy last element to index idx.
			cart.Items[len(cart.Items)-1] = ItemResource{}  // Erase last element (write zero value).
			cart.Items = cart.Items[:len(cart.Items)-1]     // Truncate slice.

			// Re-calculate price and number of items
			calcPrice(&cart)
			a.iStore[cartId] = cart

			found = true
			break
		}
	}

	if found {
		writeResponse(w, Response{
			StatusCode: http.StatusNoContent,
			Carts:      nil,
		})
	} else {
		writeError(w, http.StatusNotFound)
		return
	}
}

func calcPrice(cart *CartResource) {

	var (
		nTotal int     // Total number of items
		pTotal float64 // Total price
	)

	for _, value := range cart.Items {

		switch value.Code {
		case "PEN":
			aItem := value.Quantity / 2 // additional free items
			pTotal += products[value.Code].Price * float64(value.Quantity-aItem)
			nTotal += value.Quantity
		case "TSHIRT":
			if value.Quantity >= 3 {
				pTotal += products[value.Code].Price * float64(value.Quantity) * 0.75
				nTotal += value.Quantity
				continue
			}
			fallthrough
		default:
			pTotal += products[value.Code].Price * float64(value.Quantity)
			nTotal += value.Quantity
		}
	}

	cart.ItemsTotal = pTotal
	cart.Total = nTotal
}
