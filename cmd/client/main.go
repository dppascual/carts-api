package main

import (
	"errors"
	"fmt"
	"os"
	"os/user"
	"runtime/debug"

	"github.com/dppascual/carts-api/cmd/client/cmd"
	log "github.com/sirupsen/logrus"
)

// Errors
var (
	// ErrHomeUser is returned when the user working directory cannot be found
	ErrHomeUser = errors.New("the user working directory cannot be found")
)

func main() {
	// a panic handler. The output is logged and a "pretty" message is printed out.
	// It avoids showing any panic output to the client.
	defer func() {

		if r := recover(); r != nil {
			stack := debug.Stack()

			log.Errorf("Cause of failure: %v", r)
			log.Errorf("Error output:\n%v\n", string(stack))
			fmt.Println("lanactl encountered an unexpected error. Please see ~/lana.log")
			os.Exit(1)
		}
	}()

	// Figure out the user working directory
	u, err := user.Current()
	if err != nil {
		fmt.Printf("Error: %s", ErrHomeUser)
		os.Exit(1)
	}

	// Declare a new base command
	app, err := cmd.BaseCmd(u.HomeDir)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	// Run the main command and handle errors
	err = app.Command().Execute()
	if err != nil {
		os.Exit(1)
	}
}
