package cmd

import (
	"errors"
	"fmt"
	"io"
	"os"
	"path/filepath"

	"github.com/kelseyhightower/envconfig"

	"github.com/dppascual/carts-api/cmd/client/shared/format"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

// Default Config
const (
	// Logfile where the info is stored
	LogPath = "lana.log"
)

// const for base command
const (
	cliName            = "lanactl"
	cliDescription     = "A command line interface for Lana API Server"
	cliLongDescription = `Command line interface for Lana API Server

All of API operations can be driven through the various commands below.
For help with any of those, simply call them with --help.`
)

// Errors
var (
	// ErrInvalidNumberArgs is returned when the number of arguments is unexpected
	ErrInvalidNumberArgs = errors.New("invalid number of arguments")
	// ErrEnvProcess
	ErrEnvProcess = errors.New("cannot be processed environment variables")
)

// LanaCmd represents a command generic interface.
type LanaCmd interface {
	Command() *cobra.Command
}

// baseCmd represents the base command.
type baseCmd struct {
	homeDir string
	logFile *os.File
	apiHost string
	apiPort int

	flagHelpAll bool
	flagVerbose string
}

// BaseCmd returns an initialized BaseCmd
func BaseCmd(homeDir string) (LanaCmd, error) {

	return &baseCmd{
		homeDir: homeDir,
	}, nil
}

// Command setups and returns the carto base command.
func (b *baseCmd) Command() *cobra.Command {

	// Setup the base command.
	cmd := &cobra.Command{
		Use:          cliName,
		Short:        cliDescription,
		Long:         format.HelpSection("Description", cliLongDescription),
		SilenceUsage: true,
	}

	// Persistent flags, common across all subcommands
	cmd.PersistentFlags().StringVarP(&b.flagVerbose, "verbose", "v", log.InfoLevel.String(), "Log level (debug, info, warn, error, fatal, panic")

	// Local Flags
	cmd.Flags().BoolVar(&b.flagHelpAll, "all", false, "Show all commands")

	// Wrappers
	cmd.PersistentPreRunE = b.PreRun
	// PostRun closes the logfile file descriptor
	cmd.PersistentPostRunE = b.PostRun

	//// Register subcommands
	//
	// completion sub-command
	// carts sub-command
	cartsCmd := CartsCmd(b)
	cmd.AddCommand(cartsCmd.Command())

	// Deal with -all flag in base command
	err := cmd.ParseFlags(os.Args[1:])
	if err == nil {
		if b.flagHelpAll {
			for _, value := range cmd.Commands() {
				value.Hidden = false
			}
		}
	}
	return cmd
}

// PreRun performs several checks before a command is executed.
func (b *baseCmd) PreRun(cmd *cobra.Command, args []string) (err error) {
	// Setup the logger
	path := filepath.Join(b.homeDir, LogPath)
	file, err := os.OpenFile(path, os.O_CREATE|os.O_APPEND|os.O_RDWR, 0644)
	if err != nil {
		return fmt.Errorf("unexpected error processign the log file. %s", err)
	}
	// We can't defer file.Close() here, since the file needs to be closed after
	// all the actual commands functions have finished. So instead we store the
	// handler to b.logFile and close it on PostRun.
	b.logFile = file

	if err := setUpLogger(file, b.flagVerbose); err != nil {
		return err
	}

	// If calling the help, skip pre-run
	if cmd.Name() == "help" {
		return err
	}

	// cfg is the struct type that stores the necessary configuration
	// gathered from the environment.
	var cfg struct {
		ApiHost string `envconfig:"API_HOST" default:"localhost"`
		ApiPort int    `envconfig:"API_PORT" default:"8080"`
	}

	if err := envconfig.Process("LANA", &cfg); err != nil {
		log.WithFields(log.Fields{
			"msgErr": err,
		}).Error("cannot be processed environment variables")
		return ErrEnvProcess
	}

	// Assign environment variable to the base command
	b.apiHost = cfg.ApiHost
	b.apiPort = cfg.ApiPort

	return nil
}

func (b *baseCmd) PostRun(cmd *cobra.Command, args []string) error {
	return b.logFile.Close()
}

func (b *baseCmd) CheckArgs(args []string, minArgs int, maxArgs int) (bool, error) {
	if len(args) < minArgs || (maxArgs != -1 && len(args) > maxArgs) {
		return true, ErrInvalidNumberArgs
	}

	return false, nil
}

// setUpLogger setups the logger.
func setUpLogger(out io.Writer, level string) error {

	// Set output. By default, ~./lana.log
	log.SetOutput(out)

	// Set level log
	lvl, err := log.ParseLevel(level)
	if err != nil {
		log.WithField(
			"level", lvl,
		).Warn("Invalid log level, fallback to 'info'")
	} else {
		log.SetLevel(lvl)
	}

	// Set a formatter
	log.SetFormatter(&log.JSONFormatter{})

	return nil
}
