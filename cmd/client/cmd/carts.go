package cmd

import (
	"bytes"
	"fmt"
	"net/http"

	"github.com/dppascual/carts-api/cmd/client/shared/utils"

	"github.com/spf13/cobra"
)

// constants for carts sub-command
const (
	cartsName        = "carts"
	cartsDescription = "Display details of a set of carts or a single one"
)

// constants for carts create sub-command
const (
	cartsCreateName        = "create"
	cartsCreateDescription = "Create a new empty cart"
)

// constants for carts delete sub-command
const (
	cartsDeleteName        = "delete"
	cartsDeleteDescription = "Delete an existing cart"
	cartsDeleteExample     = `$ lanactl carts delete --cartID 1`
)

// constants for carts items sub-command
const (
	cartsItemsName        = "items"
	cartsItemsDescription = "Display details of items associated to a cart"
	cartsItemsExample     = `$ lanactl carts items --cartID 1`
)

// constants for carts items add sub-command
const (
	cartsItemsAddName        = "add"
	cartsItemsAddDescription = "Add an item to an existing cart"
	cartsItemsAddExample     = `$ lanactl carts items add --cartID 1 --code PEN --quantity 10`
)

// constants for carts items update sub-command
const (
	cartsItemsUpdateName        = "update"
	cartsItemsUpdateDescription = "Update an item of an existing cart"
	cartsItemsUpdateExample     = `$ lanactl carts items update --cartID 1 --code PEN --quantity 5`
)

// constants for carts items delete sub-command
const (
	cartsItemsDeleteName        = "delete"
	cartsItemsDeleteDescription = "Delete an item of an existing cart"
	cartsItemsDeleteExample     = `$ lanactl carts items delete --cartID 1 --code PEN`
)

// CARTS
//
// cartsCmd represents the nodes command.
type cartsCmd struct {
	base *baseCmd

	flagCartID string
}

// CartsCmd initializes an empty nodes command with the base command.
func CartsCmd(base *baseCmd) LanaCmd {
	return &cartsCmd{base: base}
}

// Command setups and returns the nodes command.
func (c *cartsCmd) Command() *cobra.Command {

	// Setup the nodes command
	cmd := &cobra.Command{
		Use:   cartsName,
		Short: cartsDescription,
	}

	cmd.RunE = c.Run

	// Local Flags
	cmd.Flags().StringVar(&c.flagCartID, "cartID", "", "ID of the Cart")

	//// Register sub-commands
	//
	// create sub-command
	cartsCreateCmd := CartsCreateCmd(c.base)
	cmd.AddCommand(cartsCreateCmd.Command())

	// delete sub-command
	cartsDeleteCmd := CartsDeleteCmd(c.base)
	cmd.AddCommand(cartsDeleteCmd.Command())

	// items sub-command
	cartsItemsCmd := CartsItemsCmd(c.base)
	cmd.AddCommand(cartsItemsCmd.Command())

	return cmd
}

// Run executes the carts sub-command.
func (c *cartsCmd) Run(cmd *cobra.Command, args []string) error {

	// Sanity checks
	exit, err := c.base.CheckArgs(args, 0, 0)
	if exit {
		return err
	}

	var url string
	if c.flagCartID == "" {
		url = fmt.Sprintf("http://%s:%d/carts", c.base.apiHost, c.base.apiPort)
	} else {
		url = fmt.Sprintf("http://%s:%d/carts/%s", c.base.apiHost, c.base.apiPort, c.flagCartID)
	}

	body, _, err := utils.DoReq(http.MethodGet, url, nil)
	if err != nil {
		return err
	}

	if len(body) != 0 {
		fmt.Println(string(body))
	} else {
		fmt.Println("no carts found")
	}

	return nil
}

// CARTS CREATE
//
// cartsCreateCmd represents the `carts create` sub-command.
type cartsCreateCmd struct {
	base *baseCmd
}

// CartsCreateCmd initializes an empty carts create command with the base command.
func CartsCreateCmd(base *baseCmd) LanaCmd {
	return &cartsCreateCmd{base: base}
}

// Command setups and returns the carts create command.
func (c *cartsCreateCmd) Command() *cobra.Command {

	// Setup the nodes install command
	cmd := &cobra.Command{
		Use:   cartsCreateName,
		Short: cartsCreateDescription,
	}

	cmd.RunE = c.Run

	//// Register sub-commands
	//

	return cmd
}

// Run executes the `carts create` sub-command.
func (c *cartsCreateCmd) Run(cmd *cobra.Command, args []string) error {

	// Sanity checks
	exit, err := c.base.CheckArgs(args, 0, 0)
	if exit {
		return err
	}

	url := fmt.Sprintf("http://%s:%d/carts", c.base.apiHost, c.base.apiPort)
	body, _, err := utils.DoReq(http.MethodPost, url, nil)
	if err != nil {
		return err
	}

	fmt.Println(string(body))
	return nil
}

// CARTS DELETE
//
// cartsDeleteCmd represents the `carts delete` sub-command.
type cartsDeleteCmd struct {
	base *baseCmd

	flagCartID string
}

// CartsDeleteCmd initializes an empty carts delete command with the base command.
func CartsDeleteCmd(base *baseCmd) LanaCmd {
	return &cartsDeleteCmd{base: base}
}

// Command setups and returns the carts delete command.
func (c *cartsDeleteCmd) Command() *cobra.Command {

	// Setup the nodes install command
	cmd := &cobra.Command{
		Use:     cartsDeleteName,
		Short:   cartsDeleteDescription,
		Example: cartsDeleteExample,
	}

	cmd.RunE = c.Run

	// Local Flags
	cmd.Flags().StringVar(&c.flagCartID, "cartID", "", "ID of the Cart (required)")
	cmd.MarkFlagRequired("cartID")

	//// Register sub-commands
	//

	return cmd
}

// Run executes the `carts delete` sub-command.
func (c *cartsDeleteCmd) Run(cmd *cobra.Command, args []string) error {

	// Sanity checks
	exit, err := c.base.CheckArgs(args, 0, 0)
	if exit {
		return err
	}

	url := fmt.Sprintf("http://%s:%d/carts/%s", c.base.apiHost, c.base.apiPort, c.flagCartID)
	body, statusCode, err := utils.DoReq(http.MethodDelete, url, nil)
	if err != nil {
		return err
	}

	if statusCode == http.StatusNoContent {
		fmt.Printf("The cart with ID '%s' has been deleted\n", c.flagCartID)
	} else {
		fmt.Println(string(body))
	}

	return nil
}

// CARTS ITEMS
//
// cartsItemsCmd represents the `carts items` sub-command.
type cartsItemsCmd struct {
	base *baseCmd
}

// CartsItemsCmd initializes an empty carts items command with the base command.
func CartsItemsCmd(base *baseCmd) LanaCmd {
	return &cartsItemsCmd{base: base}
}

// Command setups and returns the carts items command.
func (c *cartsItemsCmd) Command() *cobra.Command {

	// Setup the nodes install command
	cmd := &cobra.Command{
		Use:     cartsItemsName,
		Short:   cartsItemsDescription,
		Example: cartsItemsExample,
	}

	cmd.RunE = c.Run

	// Persistent flags, common across all sub-commands
	cmd.PersistentFlags().StringP("cartID", "", "", "ID of the Cart (required)")
	cmd.MarkPersistentFlagRequired("cartID")

	//// Register sub-commands
	//
	// add sub-command
	cartsItemsAddCmd := CartsItemsAddCmd(c.base)
	cmd.AddCommand(cartsItemsAddCmd.Command())

	// update sub-command
	cartsItemsUpdateCmd := CartsItemsUpdateCmd(c.base)
	cmd.AddCommand(cartsItemsUpdateCmd.Command())

	// delete sub-command
	cartsItemsDeleteCmd := CartsItemsDeleteCmd(c.base)
	cmd.AddCommand(cartsItemsDeleteCmd.Command())

	return cmd
}

// Run executes the `carts items` sub-command.
func (c *cartsItemsCmd) Run(cmd *cobra.Command, args []string) error {

	// Sanity checks
	exit, err := c.base.CheckArgs(args, 0, 0)
	if exit {
		return err
	}

	url := fmt.Sprintf("http://%s:%d/carts/%s", c.base.apiHost, c.base.apiPort, cmd.Flag("cartID").Value.String())
	body, _, err := utils.DoReq(http.MethodGet, url, nil)
	if err != nil {
		return err
	}

	fmt.Println(string(body))
	return nil
}

// CARTS ITEMS ADD
//
// cartsItemsAddCmd represents the `carts items add` sub-command.
type cartsItemsAddCmd struct {
	base *baseCmd

	flagCode     string
	flagQuantity int
}

// CartsItemsAddCmd initializes an empty carts items add command with the base command.
func CartsItemsAddCmd(base *baseCmd) LanaCmd {
	return &cartsItemsAddCmd{base: base}
}

// Command setups and returns the carts items add command.
func (c *cartsItemsAddCmd) Command() *cobra.Command {

	// Setup the nodes install command
	cmd := &cobra.Command{
		Use:     cartsItemsAddName,
		Short:   cartsItemsAddDescription,
		Example: cartsItemsAddExample,
	}

	cmd.RunE = c.Run

	// Local Flags
	cmd.Flags().StringVar(&c.flagCode, "code", "", "code of an item (required)")
	cmd.Flags().IntVar(&c.flagQuantity, "quantity", 0, "number of items of the chosen code to be added to the cart (required)")
	cmd.MarkFlagRequired("code")
	cmd.MarkFlagRequired("quantity")

	//// Register sub-commands
	//

	return cmd
}

// Run executes the `carts items add` sub-command.
func (c *cartsItemsAddCmd) Run(cmd *cobra.Command, args []string) error {

	// Sanity checks
	exit, err := c.base.CheckArgs(args, 0, 0)
	if exit {
		return err
	}

	url := fmt.Sprintf("http://%s:%d/carts/%s/items", c.base.apiHost, c.base.apiPort, cmd.Flag("cartID").Value.String())
	bodyReq := bytes.NewBuffer([]byte(fmt.Sprintf(`{
"variant": "%s",
"quantity": %d
}`, c.flagCode, c.flagQuantity)))
	body, _, err := utils.DoReq(http.MethodPost, url, bodyReq)
	if err != nil {
		return err
	}

	fmt.Println(string(body))
	return nil
}

// CARTS ITEMS UPDATE
//
// cartsItemsUpdateCmd represents the `carts items update` sub-command.
type cartsItemsUpdateCmd struct {
	base *baseCmd

	flagCode     string
	flagQuantity int
}

// CartsItemsUpdateCmd initializes an empty carts items update command with the base command.
func CartsItemsUpdateCmd(base *baseCmd) LanaCmd {
	return &cartsItemsUpdateCmd{base: base}
}

// Command setups and returns the carts items update command.
func (c *cartsItemsUpdateCmd) Command() *cobra.Command {

	// Setup the nodes install command
	cmd := &cobra.Command{
		Use:     cartsItemsUpdateName,
		Short:   cartsItemsUpdateDescription,
		Example: cartsItemsUpdateExample,
	}

	cmd.RunE = c.Run

	// Local Flags
	cmd.Flags().StringVar(&c.flagCode, "code", "", "code of an item (required)")
	cmd.Flags().IntVar(&c.flagQuantity, "quantity", 0, "number of items of the chosen code to be updated in the cart (required)")
	cmd.MarkFlagRequired("code")
	cmd.MarkFlagRequired("quantity")

	//// Register sub-commands
	//

	return cmd
}

// Run executes the `carts items update` sub-command.
func (c *cartsItemsUpdateCmd) Run(cmd *cobra.Command, args []string) error {

	// Sanity checks
	exit, err := c.base.CheckArgs(args, 0, 0)
	if exit {
		return err
	}

	url := fmt.Sprintf("http://%s:%d/carts/%s/items/%s",
		c.base.apiHost,
		c.base.apiPort,
		cmd.Flag("cartID").Value.String(),
		c.flagCode)
	bodyReq := bytes.NewBuffer([]byte(fmt.Sprintf(`{
"quantity": %d
}`, c.flagQuantity)))
	body, statusCode, err := utils.DoReq(http.MethodPatch, url, bodyReq)
	if err != nil {
		return err
	}

	if statusCode == http.StatusNoContent {
		fmt.Printf("The item '%s' associated to the cart with ID '%s' has been updated\n", c.flagCode, cmd.Flag("cartID").Value.String())
	} else {
		fmt.Println(string(body))
	}
	return nil
}

// CARTS ITEMS DELETE
//
// cartsItemsDeleteCmd represents the `carts items delete` sub-command.
type cartsItemsDeleteCmd struct {
	base *baseCmd

	flagCode     string
	flagQuantity int
}

// CartsItemsDeleteCmd initializes an empty carts items delete command with the base command.
func CartsItemsDeleteCmd(base *baseCmd) LanaCmd {
	return &cartsItemsDeleteCmd{base: base}
}

// Command setups and returns the carts items delete command.
func (c *cartsItemsDeleteCmd) Command() *cobra.Command {

	// Setup the nodes install command
	cmd := &cobra.Command{
		Use:     cartsItemsDeleteName,
		Short:   cartsItemsDeleteDescription,
		Example: cartsItemsDeleteExample,
	}

	cmd.RunE = c.Run

	// Local Flags
	cmd.Flags().StringVar(&c.flagCode, "code", "", "code of an item (required)")
	cmd.MarkFlagRequired("code")

	//// Register sub-commands
	//

	return cmd
}

// Run executes the `carts items delete` sub-command.
func (c *cartsItemsDeleteCmd) Run(cmd *cobra.Command, args []string) error {

	// Sanity checks
	exit, err := c.base.CheckArgs(args, 0, 0)
	if exit {
		return err
	}

	url := fmt.Sprintf("http://%s:%d/carts/%s/items/%s",
		c.base.apiHost,
		c.base.apiPort,
		cmd.Flag("cartID").Value.String(),
		c.flagCode)
	body, statusCode, err := utils.DoReq(http.MethodDelete, url, nil)
	if err != nil {
		return err
	}

	if statusCode == http.StatusNoContent {
		fmt.Printf("The item '%s' associated to the cart with ID '%s' has been deleted\n", c.flagCode, cmd.Flag("cartID").Value.String())
	} else {
		fmt.Println(string(body))
	}
	return nil
}
