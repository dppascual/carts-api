package format

import "strings"

// HelpSection indents a text section
func HelpSection(header string, content string) string {

	// Declare an empty string
	buffer := ""

	// Add a header section if it is not empty
	if header != "" {
		buffer += header + ":\n"
	}

	// Used to remove the last newline
	temp := strings.Split(content, "\n")
	lastItem := len(temp)

	for i := 0; i < lastItem; i++ {

		if temp[i] != "" {
			buffer += "  "
		}

		if i != lastItem-1 {
			buffer += temp[i] + "\n"
		} else {
			buffer += temp[i]
		}
	}

	return buffer
}
