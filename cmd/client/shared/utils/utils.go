package utils

import (
	"io"
	"io/ioutil"
	"net/http"
)

func DoReq(method, url string, body io.Reader) ([]byte, int, error) {

	// Create client
	client := &http.Client{}

	// Create request
	req, err := http.NewRequest(method, url, body)
	if err != nil {
		return nil, 0, err
	}

	// Fetch Request
	resp, err := client.Do(req)
	if err != nil {
		return nil, 0, err
	}
	defer resp.Body.Close()

	// Read Response Body
	bodyResp, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, 0, err
	}

	return bodyResp, resp.StatusCode, nil
}
