package main

import (
	"context"
	"fmt"
	"io"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/kelseyhightower/envconfig"

	log "github.com/sirupsen/logrus"

	"github.com/dppascual/carts-api/handlers"
)

func init() {
	setUpLogger(os.Stdout, "info")
}

func main() {

	// cfg is the struct type that stores the necessary configuration
	// gathered from the environment.
	var cfg struct {
		DaemonPort      int           `envconfig:"DAEMON_PORT" default:"8080"`
		ReadTimeout     time.Duration `envconfig:"READ_TIMEOUT" default:"5s"`
		WriteTimeout    time.Duration `envconfig:"WRITE_TIMEOUT" default:"10s"`
		ShutdownTimeout time.Duration `envconfig:"SHUTDOWN_TIMEOUT" default:"5s"`
	}

	if err := envconfig.Process("LANA", &cfg); err != nil {
		log.WithFields(log.Fields{
			"msgErr": err,
		}).Error("cannot be processed environment variables")
		return
	}

	// Declare a new App
	app := handlers.NewApp()

	// Start goroutine responsible for handling interaction with
	// the carts map
	// TODO: Remove when the map stored in memory to be substituted
	//  by a database.
	go app.StartManager()

	server := http.Server{
		Addr:           fmt.Sprintf(":%d", cfg.DaemonPort),
		Handler:        app,
		ReadTimeout:    cfg.ReadTimeout,
		WriteTimeout:   cfg.WriteTimeout,
		MaxHeaderBytes: 1 << 20,
	}

	// Start listening for requests made to the daemon and create a channel
	// to collect non-HTTP related server errors on.
	serverErrors := make(chan error, 1)
	go func() {
		log.Printf("server started, listening on %s", server.Addr)
		serverErrors <- server.ListenAndServe()
	}()

	// Blocking main and waiting for shutdown of the daemon.
	osSignals := make(chan os.Signal, 1)
	signal.Notify(osSignals, os.Interrupt, syscall.SIGTERM)

	// Waiting for an osSignal or a non-HTTP related server error.
	select {
	case err := <-serverErrors:
		log.WithFields(log.Fields{
			"msgErr": err,
		}).Error("server failed to start")
		return

	case <-osSignals:
	}

	// Gracefully shutdown server once an exit signal or error is received.
	ctx, cancel := context.WithTimeout(context.Background(), cfg.ShutdownTimeout)
	defer cancel()

	log.Info("shutdown: Starting graceful shutdown...")
	if err := server.Shutdown(ctx); err != nil {
		log.WithFields(log.Fields{
			"msgErr": err,
		}).Error("shutdown: Graceful shutdown did not complete in ", cfg.ShutdownTimeout)

		log.Info("shutdown: Starting killing server...")
		if err := server.Close(); err != nil {
			log.WithFields(log.Fields{
				"msgErr": err,
			}).Error("shutdown: Error killing server")
			return
		}
	}
	log.Info("shutdown: OK")
}

func setUpLogger(dst io.Writer, level string) {

	// Set output. By default, stdout
	log.SetOutput(dst)

	// Set level
	lvl, err := log.ParseLevel(level)
	if err != nil {
		log.WithField(
			"level", lvl,
		).Warn("Invalid log level, fallback to info")
		log.SetLevel(log.InfoLevel)
	} else {
		log.SetLevel(lvl)
	}

	// Set formatter
	log.SetFormatter(&log.JSONFormatter{})
}
